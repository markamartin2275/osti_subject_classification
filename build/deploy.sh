#!/bin/bash

# generally a good idea to more surgically move stuff up, as there are
# some really large files we need to move when moving everything
scp -P $MARKAMARTIN_SSH_PORT -r ./dist/* "$MARKAMARTIN_USER_HOST":"$D2_PATH_TO_APP"