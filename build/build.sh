#!/bin/bash

# wipe and rebuild
rm -rf ./dist/
mkdir -p ./dist/

#be more selective than this
cp README.md ./dist/
cp requirements.txt ./dist/
cp *.py ./dist/
cp subject_field.pt ./dist/
cp text_field.pt ./dist/
cp -R ./models/ ./dist/