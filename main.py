import argparse
import os
import torch

from torchtext import data
from torchtext import datasets

from data_preprocessing import data_preprocessing

from structures import sourcedata
from structures import predictions

from models.bert.bert_main import bert_main
from models.cnn.cnn_main import cnn_main
from models.fasttext.fasttext_main import fasttext_main
from models.lstm.lstm_main import lstm_main

#
# activate the venv:  source venv/bin/activate
# example command line call:
# python -W ignore main.py --model all --mode predict --classification-text="This work quantified nonuniform creep deformation across the heterogeneous heat-affected zone (HAZ) of Grade 91 steel with sophisticated experiments, including an electric-thermal finite element model–assisted Gleeble thermomechanical simulation and a high-temperature creep testing with in situ digital image correlation (DIC). High temperature creep properties of HAZ sub-zones were quantitatively measured by the DIC. Furthermore, by utilizing peak temperature, hardness, local creep strain, and underlying microstructures, creep deformation mechanisms in HAZ were further understood. DIC measurements reveal a creep-vulnerable zone (CVZ) exposed to a peak temperature of 932°C (close to A C3) in the intercritical HAZ experienced the fastest creep strength degradation instead of the soft zone with the lowest hardness prior to creep. The significantly reduced precipitation strengthening from misplacement of undissolved and coarsened M 23C 6 carbides led to a faster recrystallization of tempered martensite in the CVZ. Weak untransformed tempered martensite (ferrite grains) stabilized by local Cr enrichment from dissolved M 23C 6 also harmed the CVZ's creep resistance."
# 

# define command line parser and arguments that are supported
parser = argparse.ArgumentParser(description='OSTI Scientific and Technical Information Subject Classification')

# Execution mode related arguments
parser.add_argument('--mode', default='', type=str, help='execution mode (default: train; valid values: train | predict | preprocess-data)')
parser.add_argument('--model', default='', type=str, help='activated model (default: all; valid values: all | bert | cnn | fasttext | lstm)')
parser.add_argument('--classification-text', default='', type=str, help='text to classify when in predict mode (default: none)')


def main():

    args = parser.parse_args()

    initdata = sourcedata()

    if args.mode == "preprocess-data":
        print("begin data preprocessing ...")
        data_preprocessing()
        print("completed data preporcessing")
        quit()

    if args.model == "all" or args.model == "bert":
        bert_main(args.mode, initdata, args.classification_text)
    
    if args.model == "all" or args.model == "cnn":
        cnn_main(args.mode, initdata, args.classification_text)
    
    #if args.model == "all" or args.model == "fasttext":
        #fasttext_main(args.mode, initdata, args.classification_text)
    
    if args.model == "all" or args.model == "lstm":
        lstm_main(args.mode, initdata, args.classification_text)
    

if __name__ == "__main__":
    main()

