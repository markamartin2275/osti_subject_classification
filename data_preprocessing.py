import re
import json
import os
import pdb
import spacy

sp = spacy.load('en_core_web_sm')
all_stopwords = sp.Defaults.stop_words
# identified by reviewing TEXT.vocab most frequent tokens
additional_stopwords = ["the", "this", "in", "we", "report", "for", ",", " ", "}", "00:0", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]

def remove_stopwords(line):
  tokens = line.split(" ")
  tokens_filtered = [word.lower() for word in tokens if not word.lower() in all_stopwords]
  tokens_filtered = [word.lower() for word in tokens_filtered if not word.lower() in additional_stopwords]
  return (" ").join(tokens_filtered)

# assume the project is being run from project root directory and these data files exist
# the input_data_files were generated via https://markamartin.io/gitlab/go/go-data/-/tree/master/osti-subject-classification
#
#input_data_files = ["./data/osti-1K.json", "./data/osti-10K.json", "./data/osti-100K.json", "./data/osti-3M.json"]
#output_train_files = ["./data/osti-1K-train.json", "./data/osti-10K-train.json", "./data/osti-100K-train.json", "./data/osti-3M-train.json"]
#output_valid_files = ["./data/osti-1K-valid.json", "./data/osti-10K-valid.json", "./data/osti-100K-valid.json", "./data/osti-3M-valid.json"]
#output_test_files = ["./data/osti-1K-test.json", "./data/osti-10K-test.json", "./data/osti-100K-test.json", "./data/osti-3M-test.json"]
input_data_files = ["./data/osti-1K.json"]
output_train_files = ["./data/osti-1K-train.json"]
output_valid_files = ["./data/osti-1K-valid.json"]
output_test_files = ["./data/osti-1K-test.json"]

# process our input data files ...
def data_preprocessing():
  for i in range(len(input_data_files)):
    train_file = open(output_train_files[i],'w')
    valid_file = open(output_valid_files[i],'w')
    test_file = open(output_test_files[i],'w')
    input_file =  open(input_data_files[i], 'r')
    counter = 0
    for line in input_file:
      try:
        # cycling every 10 records placing records in train, valid, and test data files
        if counter > 10:
          counter = 0
        # remove non alphanumeric character, excluding those that are needed in the markup of the json:
        line = re.sub(r'[^A-Za-z0-9":{},_]+', ' ', line) 
        # remove common stop words using spacy stop word list and our custom stop word list
        line = remove_stopwords(line)
        #print(line)
        json.JSONDecoder(strict=True).decode(line)
        json_line = json.loads(line)

        # grab individual fields
        subject = json_line["subject"]
        title = re.sub(r'[^A-Za-z0-9]+', ' ', str(json_line["title"]))
        description = re.sub(r'[^A-Za-z0-9]+', ' ', str(json_line["description"]))
        product_type = re.sub(r'[^A-Za-z0-9]+', ' ', str(json_line["product_type"]))
        publication_date = str(json_line["publication_date"])[0:4] # just the year
        report_number = "" # currently not using report number
        #report_number = json_line["report_number"]
        
        # limit the records we add to only those that have a single subject assigned and that
        # subject is part of the controlled subject vocabulary as identified by leading two characters being
        # numeric followed by a single space - the len < 75 attempts to limit to single entires not multiple
        # subjects from the controlled vocabulary being assigned
        if len(subject) > 3:
          if subject[0].isdigit() and subject[1].isdigit() and subject[2] == " " and len(subject) < 75:
            counter += 1
            outputline = "{\"text\":\"" + title + " " + \
              description + " " + product_type + " " + \
              publication_date + " " + report_number + \
              "\", \"subject\":\"" + subject + \
              "\"}\n"

            #print(outputline)

            # create pseudo-random splits in the files
            if counter == 1:
              train_file.write(outputline)
            elif counter == 2:
              valid_file.write(outputline)
            elif counter == 3:
              train_file.write(outputline)
            elif counter == 4:
              test_file.write(outputline)
            elif counter == 5:
              train_file.write(outputline)
            elif counter == 6:
              valid_file.write(outputline)
            else:
              train_file.write(outputline)
      
      except json.JSONDecodeError:
        pass
      # handle additional poorly formed JSON that makes it through the decoder
      except TypeError:
        pass
        #print("Skipping: ")
        #print(line)