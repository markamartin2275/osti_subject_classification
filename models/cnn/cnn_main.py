import os
import datetime
import torch
import torch.nn as nn
from torchtext import data

from structures import sourcedata
from structures import predictions

from models.cnn.cnn_train_evaluate import run_train, evaluate, create_iterator
from models.cnn.cnn_model import CNN


def cnn_main(mode, initdata, classification_text):

    # enable cuda if available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # ensure we reproduce the same results
    SEED = 2020
    torch.manual_seed(SEED)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.enabled = True
    
    # Hyperparamters
    NUM_EPOCHS = 20
    BATCH_SIZE = 350
    INPUT_DIM = len(initdata.TEXT.vocab)
    EMBEDDING_DIM = 300
    N_FILTERS = 100
    FILTER_SIZES = [3,5,7,9]
    OUTPUT_DIM = len(initdata.SUBJECT.vocab)
    DROPOUT = 0.5
    WEIGHT_DECAY = 0.00001
    PAD_IDX = initdata.TEXT.vocab.stoi[initdata.TEXT.pad_token]

    cnn = CNN(INPUT_DIM, EMBEDDING_DIM, N_FILTERS, FILTER_SIZES, OUTPUT_DIM, DROPOUT, PAD_IDX)
    cnn.to(device)


    pretrained_embeddings = initdata.TEXT.vocab.vectors
    cnn.embedding.weight.data.copy_(pretrained_embeddings)
    UNK_IDX = initdata.TEXT.vocab.stoi[initdata.TEXT.unk_token]
    cnn.embedding.weight.data[UNK_IDX] = torch.zeros(EMBEDDING_DIM)
    cnn.embedding.weight.data[PAD_IDX] = torch.zeros(EMBEDDING_DIM)


    if mode == "train":
        # create iterators
        train_iterator, valid_iterator, test_iterator = create_iterator(initdata.train_data, initdata.valid_data, initdata.test_data, BATCH_SIZE, device)

        # load last state
        #lstm.load_state_dict(torch.load(os.path.abspath("./saved_weights_"+MODEL_TYPE+".pt")))

        return cnn_train(NUM_EPOCHS, cnn, train_iterator, valid_iterator, test_iterator, WEIGHT_DECAY)
    
    elif mode == "predict":
        # load state dictionary
        if torch.cuda.is_available():
            cnn.load_state_dict(torch.load(os.path.abspath("./models/cnn/saved_weights_cnn.pt.best")))
        else:
            cnn.load_state_dict(torch.load(os.path.abspath("./models/cnn/saved_weights_cnn.pt.best"), map_location=device))
        return cnn_predict(cnn, classification_text, initdata, device)
 

# train the network
def cnn_train(NUM_EPOCHS, cnn, train_iterator, valid_iterator, test_iterator, weight_decay):   
    
    optimizer = optim.Adam(cnn.parameters(), weight_decay)
    loss_func = nn.CrossEntropyLoss()
    
    # train
    print(str(datetime.datetime.now()))
    run_train(NUM_EPOCHS, cnn, train_iterator, valid_iterator, optimizer, loss_func)
    print(str(datetime.datetime.now()))

    # load the best weights
    cnn.load_state_dict(torch.load(os.path.abspath("./models/cnn/saved_weights_cnn.pt")))
    
    # test
    test_loss, test_acc = evaluate(cnn, test_iterator, loss_func)
    print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc * 100:.2f}%')

    return ""


# prediction
def convert_input_text_to_glove_vector(text, initdata, device):
    tokenized = [tok for tok in text.split()]
    indexed = [initdata.TEXT.vocab.stoi[t] for t in tokenized]
    tensor = torch.LongTensor(indexed).to(device)
    return tensor.unsqueeze(1)

def cnn_predict(cnn, prediction_text, initdata, device):

    preds = predictions()

    # glove embedding of the prediction text
    prediction_text_vector = convert_input_text_to_glove_vector(prediction_text, initdata, device)

    cnn.eval()
    model_predictions = cnn(prediction_text_vector)

    top3 = torch.topk(model_predictions, 3)
    indices = top3.indices
    indices.to("cpu")
    indices.detach()
    values = top3.values
    values.to("cpu")
    values.detach()
    
    preds.prediction_one = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][0]]
    preds.prediction_one_probability = values.cpu().detach().numpy()[0][0]
    print("CNN Pred 1: ", preds.prediction_one)
    print (f"CNN Pred 1 Probability: {preds.prediction_one_probability:.2f}%")
    
    preds.prediction_two = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][1]]
    preds.prediction_two_probability = values.cpu().detach().numpy()[0][1]
    print("CNN Pred 2: ", preds.prediction_two)
    print (f"CNN Pred 2 Probability: {preds.prediction_two_probability:.2f}%")
    
    preds.prediction_three = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][2]]
    preds.prediction_three_probability = values.cpu().detach().numpy()[0][2]
    print("CNN Pred 3: ", preds.prediction_three)
    print (f"CNN Pred 3 Probability: {preds.prediction_three_probability:.2f}%")

    return preds
