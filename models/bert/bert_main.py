import torch
import torch.nn as nn
from torchtext import data
from torchtext import datasets
import random
import numpy as np
import torch.optim as optim
import random
import re
import json
import os
import datetime

from structures import sourcedata
from structures import predictions

from transformers import AutoTokenizer, AutoModel
from models.bert.bert_train_evaluate import run_train, evaluate, create_iterator
from models.bert.bert_model import SciBERT

# Transformers Tokenizer and Model - AllenAI SciBert SciVocab Uncased via AutoTokenizer and AutoModel
scibert_tokenizer = AutoTokenizer.from_pretrained('allenai/scibert_scivocab_uncased')

init_token = scibert_tokenizer.cls_token
eos_token = scibert_tokenizer.sep_token
pad_token = scibert_tokenizer.pad_token
unk_token = scibert_tokenizer.unk_token

init_token_idx = scibert_tokenizer.convert_tokens_to_ids(init_token)
eos_token_idx = scibert_tokenizer.convert_tokens_to_ids(eos_token)
pad_token_idx = scibert_tokenizer.convert_tokens_to_ids(pad_token)
unk_token_idx = scibert_tokenizer.convert_tokens_to_ids(unk_token)

# http://www.arxiv-vanity.com/papers/1903.10676/
# max sentence length is 512; also happens to be the limit for bert-base-uncased; using that
max_input_length = scibert_tokenizer.max_model_input_sizes['bert-base-uncased']

def tokenize_and_cut(text):
    tokens = scibert_tokenizer.tokenize(text) 
    tokens = tokens[:max_input_length-2]
    return tokens



def bert_main(mode, initdata, classification_text):
    
    # enable cuda if available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # ensure we reproduce the same results
    SEED = 2020
    torch.manual_seed(SEED)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.enabled = True

    # pretrained allenai/scibert that we'll pass as input to our scibert model
    allenai_scibert = AutoModel.from_pretrained("allenai/scibert_scivocab_uncased")

    # Hyperparamters
    NUM_EPOCHS = 10
    BATCH_SIZE = 32
    HIDDEN_DIM = 300
    NUM_CLASSES = len(initdata.SUBJECT.vocab)
    N_LAYERS = 2
    BIDIRECTIONAL = True
    DROPOUT = 0
    WEIGHT_DECAY = 0

    scibert = SciBERT(allenai_scibert, HIDDEN_DIM, NUM_CLASSES, N_LAYERS, BIDIRECTIONAL, DROPOUT)
    scibert.to(device)

    # don't want to train the pretrained parameters
    for name, param in scibert.named_parameters():                
        if name.startswith('bert'):
            param.requires_grad = False

    if mode == "train":
        # create iterators
        train_iterator, valid_iterator, test_iterator = create_iterator(initdata.train_data, initdata.valid_data, initdata.test_data, BATCH_SIZE, device)

        # load last state
        #lstm.load_state_dict(torch.load(os.path.abspath("./saved_weights_"+MODEL_TYPE+".pt")))

        return bert_train(NUM_EPOCHS, scibert, train_iterator, valid_iterator, test_iterator, WEIGHT_DECAY)
    
    elif mode == "predict":
        # load state dictionary
        if torch.cuda.is_available():
            scibert.load_state_dict(torch.load(os.path.abspath("./models/bert/saved_weights_scibert.pt.best")))
        else:
            scibert.load_state_dict(torch.load(os.path.abspath("./models/bert/saved_weights_scibert.pt.best"), map_location=device))
        return bert_predict(scibert, scibert_tokenizer, classification_text, initdata, device)
 

# train the network
def bert_train(NUM_EPOCHS, scibert, train_iterator, valid_iterator, test_iterator, weight_decay):   
    
    optimizer = optim.Adam(scibert.parameters(), weight_decay=weight_decay)
    loss_func = nn.CrossEntropyLoss()
    
    # train
    print(str(datetime.datetime.now()))
    run_train(NUM_EPOCHS, scibert, train_iterator, valid_iterator, optimizer, loss_func)
    print(str(datetime.datetime.now()))

    # load the best weights
    scibert.load_state_dict(torch.load(os.path.abspath("./models/bert/saved_weights_scibert.pt")))
    
    # test
    test_loss, test_acc = evaluate(scibert, test_iterator, loss_func)
    print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc * 100:.2f}%')

    return ""

# prediction
def predict_sentiment(model, tokenizer, device, sentence):
    
    model.eval()
    tokens = tokenizer.tokenize(sentence)
    tokens = tokens[:max_input_length-2]
    indexed = [init_token_idx] + tokenizer.convert_tokens_to_ids(tokens) + [eos_token_idx]
    tensor = torch.LongTensor(indexed).to(device)
    tensor = tensor.unsqueeze(0)
    prediction = torch.sigmoid(model(tensor))
    return prediction

def bert_predict(scibert, scibert_tokenizer, prediction_text, initdata, device):

    preds = predictions()
    
    scibert.eval()
    model_predictions = predict_sentiment(scibert, scibert_tokenizer, device, prediction_text)

    top3 = torch.topk(model_predictions, 3)
    indices = top3.indices
    indices.to("cpu")
    indices.detach()
    values = top3.values
    values.to("cpu")
    values.detach()
    
    preds.prediction_one = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][0]]
    preds.prediction_one_probability = values.cpu().detach().numpy()[0][0]
    print("SciBERT Pred 1: ", preds.prediction_one)
    print (f"SciBERT Pred 1 Probability: {preds.prediction_one_probability:.2f}%")
    
    preds.prediction_two = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][1]]
    preds.prediction_two_probability = values.cpu().detach().numpy()[0][1]
    print("SciBERT Pred 2: ", preds.prediction_two)
    print (f"SciBERT Pred 2 Probability: {preds.prediction_two_probability:.2f}%")
    
    preds.prediction_three = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][2]]
    preds.prediction_three_probability = values.cpu().detach().numpy()[0][2]
    print("SciBERT Pred 3: ", preds.prediction_three)
    print (f"SciBERT Pred 3 Probability: {preds.prediction_three_probability:.2f}%")

    return preds