import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from torchtext import data
from torchtext import datasets
import torch.optim as optim
import random
import re
import json
import os
import datetime
from models.fasttext.fasttext_train_evaluate import run_train, evaluate, create_iterator
from models.fasttext.fasttext_model import FastText


def generate_bigrams(x):
    n_grams = set(zip(*[x[i:] for i in range(2)]))
    for n_gram in n_grams:
        x.append(' '.join(n_gram))
    return x

# ensure we reproduce the same results
SEED = 2020
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
torch.backends.cudnn.enabled = True

# input data
TEXT = data.Field(tokenize='spacy',lower=True,preprocessing = generate_bigrams)
# label data
SUBJECT = data.LabelField(sequential=False, lower=True)
# fields
fields = {
  'text': ('text', TEXT),
  'subject': ('subject', SUBJECT)
}

# enable cuda if available
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def fasttext_main(mode, initdata, classification_text):

    # enable cuda if available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # ensure we reproduce the same results
    SEED = 2020
    torch.manual_seed(SEED)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.enabled = True

    # Hyperparamters
    NUM_EPOCHS = 10
    BATCH_SIZE = 128
    INPUT_DIM = len(TEXT.vocab)
    EMBEDDING_DIM = 300
    OUTPUT_DIM = len(SUBJECT.vocab)
    PAD_IDX = TEXT.vocab.stoi[TEXT.pad_token]
    

    # create fastText model and put it on the GPU if available
    fasttext = FastText(INPUT_DIM, EMBEDDING_DIM, OUTPUT_DIM, PAD_IDX)
    fasttext.to(device)


    # add pretained embeddings to the model
    pretrained_embeddings = initdata.TEXT.vocab.vectors
    fasttext.embedding.weight.data.copy_(pretrained_embeddings)
    UNK_IDX = initdata.TEXT.vocab.stoi[TEXT.unk_token]
    fasttext.embedding.weight.data[UNK_IDX] = torch.zeros(EMBEDDING_DIM)
    fasttext.embedding.weight.data[PAD_IDX] = torch.zeros(EMBEDDING_DIM)


    if mode == "train":
        # create iterators
        train_iterator, valid_iterator, test_iterator = create_iterator(initdata.train_data, initdata.valid_data, initdata.test_data, BATCH_SIZE, device)

        # load last state
        #lstm.load_state_dict(torch.load(os.path.abspath("./saved_weights_"+MODEL_TYPE+".pt")))

        fasttext_train(NUM_EPOCHS, fasttext, train_iterator, valid_iterator, test_iterator)
    
    elif mode == "predict":
        # load state dictionary
        if torch.cuda.is_available():
            fasttext.load_state_dict(torch.load(os.path.abspath("./models/lstm/saved_weights_fasttext.pt.best")))
        else:
            fasttext.load_state_dict(torch.load(os.path.abspath("./models/lstm/saved_weights_fasttext.pt.best"), map_location=device))
        fasttext_predict(fasttext)
 

# train the network
def fasttext_train(NUM_EPOCHS, fasttext, train_iterator, valid_iterator, test_iterator):   
    
    optimizer = optim.Adam(fasttext.parameters())
    loss_func = nn.CrossEntropyLoss()
    
    # train
    print(str(datetime.datetime.now()))
    run_train(NUM_EPOCHS, fasttext, train_iterator, valid_iterator, optimizer, loss_func, MODEL_TYPE)
    print(str(datetime.datetime.now()))

    # load the best weights
    fasttext.load_state_dict(torch.load(os.path.abspath("./saved_weights_"+MODEL_TYPE+".pt")))
    
    # test
    test_loss, test_acc = evaluate(fasttext, test_iterator, loss_func)
    print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc * 100:.2f}%')


# prediction
def convert_input_text_to_glove_vector(text):
    tokenized = [tok for tok in text.split()]
    tokenized += generate_bigrams(tokenized)
    indexed = [TEXT.vocab.stoi[t] for t in tokenized]
    tensor = torch.LongTensor(indexed).to(device)
    return tensor.unsqueeze(1)

def fasttext_predict(fasttext):

    # prediction text
    #test_text = "The Nuclear Science User Facilities (NSUF) is the United States Department of Energy Office of Nuclear Energy's only designated nuclear energy user facility. Its mission is to provide nuclear energy researchers access to world-class capabilities and to facilitate the advancement of nuclear science and technology. This mission is supported by providing access to state-of-the-art experimental irradiation testing, post-irradiation examination facilities, and high-performance computing capabilities as well as technical and scientific assistance for the design and execution of projects. Two NSUF irradiation experiments, Boise State University-8242 and General Electric Hitachi, used melt wire packs to determine peak temperatures reached during irradiation testing. This report will discuss the temperature evaluation process and present the associated data."
    prediction_text = "This is a paper covering the exciting scientific development of new materials through extensive research on polymers and chemistry 1998"

    # glove embedding of the prediction text
    prediction_text_vector = convert_input_text_to_glove_vector(prediction_text)

    fasttext.eval()
    predictions = fasttext(prediction_text_vector)
    top2 = torch.topk(predictions, 2)
    indices = top2.indices
    values = top2.values
    pred1 = indices.cpu().detach().numpy()[0][0]
    pred1prob = values.cpu().detach().numpy()[0][0]
    print("Pred 1: ", SUBJECT.vocab.itos[pred1])
    print (f"Pred 1 Probability: {pred1prob:.2f}%")
    pred2 = indices.cpu().detach().numpy()[0][1]
    pred2prob = values.cpu().detach().numpy()[0][1]
    print("Pred 2: ", SUBJECT.vocab.itos[pred2])
    print (f"Pred 2 Probability: {pred2prob:.2f}%")
