import os
import datetime
import torch
import torch.nn as nn
from torchtext import data

from structures import sourcedata
from structures import predictions

from models.lstm.lstm_train_evaluate import run_train, evaluate, create_iterator
from models.lstm.lstm_model import LSTM



def lstm_main(mode, initdata, prediction_text):

    # enable cuda if available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # ensure we reproduce the same results
    SEED = 2020
    torch.manual_seed(SEED)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.enabled = True

    # hyperparamters
    NUM_EPOCHS = 5
    BATCH_SIZE = 256
    VOCAB_SIZE =  len(initdata.TEXT.vocab)
    NUM_CLASSES = len(initdata.SUBJECT.vocab)
    EMBED_DIM = 300
    HIDDEN_DIM1= 300
    NUM_LAYERS = 2
    BIDIRECTIONAL = True
    DROPOUT = 0.1
    PAD_IDX = initdata.TEXT.vocab.stoi[initdata.TEXT.pad_token]

    # create LSTM RNN and put it on the GPU if available
    lstm = LSTM(VOCAB_SIZE, EMBED_DIM, HIDDEN_DIM1, NUM_CLASSES, NUM_LAYERS, BIDIRECTIONAL, DROPOUT, PAD_IDX)
    lstm.to(device)

    #def count_parameters(model):
    #    return sum(p.numel() for p in model.parameters() if p.requires_grad)
    #print(f'The model has {count_parameters(lstm):,} trainable parameters')

    # load the pretrained glove embeddings into the model - https://colab.research.google.com/github/bentrevett/pytorch-sentiment-analysis/blob/master/2%20-%20Upgraded%20Sentiment%20Analysis.ipynb#scrollTo=r1OnAbgQZPld
    glove_embeddings = initdata.TEXT.vocab.vectors
    lstm.embedding.weight.data.copy_(glove_embeddings)
    UNK_IDX = initdata.TEXT.vocab.stoi[initdata.TEXT.unk_token]
    lstm.embedding.weight.data[UNK_IDX] = torch.zeros(EMBED_DIM)
    lstm.embedding.weight.data[PAD_IDX] = torch.zeros(EMBED_DIM)
    
    if mode == "train":

        # create iterators
        train_iterator, valid_iterator, test_iterator = create_iterator(initdata.train_data, initdata.valid_data, initdata.test_data, BATCH_SIZE, device)

        # load last state
        #lstm.load_state_dict(torch.load(os.path.abspath("./saved_weights_"+MODEL_TYPE+".pt")))

        return lstm_train(NUM_EPOCHS, lstm, train_iterator, valid_iterator, test_iterator)
    
    elif mode == "predict":
        # load last state
        if torch.cuda.is_available():
            lstm.load_state_dict(torch.load(os.path.abspath("./models/lstm/saved_weights_bidirectional_lstm.pt.best")))
        else:
            lstm.load_state_dict(torch.load(os.path.abspath("./models/lstm/saved_weights_bidirectional_lstm.pt.best"), map_location=device))
        return lstm_predict(lstm, prediction_text, initdata, device)
 

# train the network
def lstm_train(NUM_EPOCHS, lstm, train_iterator, valid_iterator, test_iterator):   
    
    loss_func = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(lstm.parameters())
    
    # train
    print(str(datetime.datetime.now()))
    run_train(NUM_EPOCHS, lstm, train_iterator, valid_iterator, optimizer, loss_func)
    print(str(datetime.datetime.now()))

    # load the best weights
    lstm.load_state_dict(torch.load(os.path.abspath("./models/lstm/saved_weights_bidirectional_lstm.pt")))
    
    # test
    test_loss, test_acc = evaluate(lstm, test_iterator, loss_func)
    print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc * 100:.2f}%')

    return ""


# prediction
def convert_input_text_to_glove_vector(text, initdata, device):
    tokenized = [tok for tok in text.split()]
    indexed = [initdata.TEXT.vocab.stoi[t] for t in tokenized]
    indexed_lengths = len(indexed)
    tensor = torch.LongTensor(indexed).to(device)
    lengths_tensor = torch.LongTensor([indexed_lengths])
    return tensor.unsqueeze(1), lengths_tensor

def lstm_predict(lstm, prediction_text, initdata, device):

    preds = predictions()
    
    # glove embedding of the prediction text
    prediction_text_vector, prediction_text_lengths_vector = convert_input_text_to_glove_vector(prediction_text, initdata, device)

    lstm.eval()
    model_predictions = lstm(prediction_text_vector, prediction_text_lengths_vector)
    
    top3 = torch.topk(model_predictions, 3)
    indices = top3.indices
    indices.to("cpu")
    indices.detach()
    values = top3.values
    values.to("cpu")
    values.detach()
    
    preds.prediction_one = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][0]]
    preds.prediction_one_probability = values.cpu().detach().numpy()[0][0]
    print("LSTM Pred 1: ", preds.prediction_one)
    print (f"LSTM Pred 1 Probability: {preds.prediction_one_probability:.2f}%")
    
    preds.prediction_two = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][1]]
    preds.prediction_two_probability = values.cpu().detach().numpy()[0][1]
    print("LSTM Pred 2: ", preds.prediction_two)
    print (f"LSTM Pred 2 Probability: {preds.prediction_two_probability:.2f}%")
    
    preds.prediction_three = initdata.SUBJECT.vocab.itos[indices.cpu().detach().numpy()[0][2]]
    preds.prediction_three_probability = values.cpu().detach().numpy()[0][2]
    print("LSTM Pred 3: ", preds.prediction_three)
    print (f"LSTM Pred 3 Probability: {preds.prediction_three_probability:.2f}%")

    return preds
