import json
import os
import torch

from torchtext import data
from torchtext import datasets

# ....
class sourcedata():

    def __init__(self):
        
        # input
        self.TEXT = data.Field(tokenize='spacy',lower=True,include_lengths=True)
        
        # label
        self.SUBJECT = data.LabelField(sequential=False, lower=True)
        
        
        self.fields = {
        'text': ('text', self.TEXT),
        'subject': ('subject', self.SUBJECT)
        }

        self.TEXT.__dict__ = torch.load(os.path.abspath("./text_field.pt"))
        self.SUBJECT.__dict__ = torch.load(os.path.abspath("./subject_field.pt"))
        

class predictions():

    def __init__(self):
        self.prediction_one = ""
        self.prediction_one_probability = 0.0
        self.prediction_two = ""
        self.prediction_two_probability = 0.0
        self.prediction_three = ""
        self.prediction_three_probability = 0.0