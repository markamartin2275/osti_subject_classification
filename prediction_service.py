from flask import Flask, render_template, request, Response, after_this_request
import json
import logging
import os
import re

from structures import sourcedata
from structures import predictions

from models.bert.bert_main import bert_main
from models.cnn.cnn_main import cnn_main
#from models.fasttext.fasttext_main import fasttext_main
from models.lstm.lstm_main import lstm_main


#
# activate the venv:  source venv/bin/activate
# example command line call: python -W ignore prediction_service.py
# requesting from API:
#  curl -G 'http://localhost:8644/prediction_service' --data-urlencode "prediction_text=This work quantified nonuniform creep deformation across the heterogeneous heat-affected zone (HAZ) of Grade 91 steel with sophisticated experiments, including an electric-thermal finite element model–assisted Gleeble thermomechanical simulation and a high-temperature creep testing with in situ digital image correlation (DIC). High temperature creep properties of HAZ sub-zones were quantitatively measured by the DIC. Furthermore, by utilizing peak temperature, hardness, local creep strain, and underlying microstructures, creep deformation mechanisms in HAZ were further understood. DIC measurements reveal a creep-vulnerable zone (CVZ) exposed to a peak temperature of 932°C (close to A C3) in the intercritical HAZ experienced the fastest creep strength degradation instead of the soft zone with the lowest hardness prior to creep. The significantly reduced precipitation strengthening from misplacement of undissolved and coarsened M 23C 6 carbides led to a faster recrystallization of tempered martensite in the CVZ. Weak untransformed tempered martensite (ferrite grains) stabilized by local Cr enrichment from dissolved M 23C 6 also harmed the CVZ's creep resistance."
# 

# get a Flask app
app = Flask(__name__)

# turn off the debugger
app.config["DEBUG"] = False

# configure logging
logdir_exists = os.path.isdir("logs")
# if folder doesn't exist, then create it
if not logdir_exists:
    os.makedirs("logs")
logging.basicConfig(filename='./logs/prediction_service.log', level=logging.DEBUG)

initdata = sourcedata()

# main route
@app.route('/prediction_service', methods = ['GET', 'POST'])
def predict():
    # add a few headers on the way out
    # need the Access-Control-Allow-Origin specifically for dev/testing
    # with localhost
    @after_this_request
    def add_header(response):
        response.headers.add('Access-Control-Allow-Origin', '*')
        response.headers.add("Content-Type", "application/json; charset=utf-8")
        return response
    
    prediction_text = ""
    
    if request.method == "GET":
        prediction_text = request.args.get('prediction_text') + ""
    
    if request.method == 'POST':
        prediction_text = request.form.get('prediction_text') + ""

    print("Prediction Text = " + prediction_text)
    prediction_text_length = len(re.findall(r'\w+', prediction_text))

    lstm_predictions = lstm_main("predict", initdata, prediction_text)
    
    # cnn model requires at least 9 tokens in prediction text to execute without error
    if prediction_text_length > 8:
        cnn_predictions = cnn_main("predict", initdata, prediction_text)
    
    bert_predictions = bert_main("predict", initdata, prediction_text)
    
    if prediction_text_length > 8:
        response = "{"
        response += "\"lstm_prediction_one\":\""+lstm_predictions.prediction_one+"\","
        response += "\"lstm_prediction_one_probability\":\""+str(lstm_predictions.prediction_one_probability)+"\","
        response += "\"lstm_prediction_two\":\""+lstm_predictions.prediction_two+"\","
        response += "\"lstm_prediction_two_probability\":\""+str(lstm_predictions.prediction_two_probability)+"\","
        response += "\"lstm_prediction_three\":\""+lstm_predictions.prediction_three+"\","
        response += "\"lstm_prediction_three_probability\":\""+str(lstm_predictions.prediction_three_probability)+"\","
        response += "\"cnn_prediction_one\":\""+cnn_predictions.prediction_one+"\","
        response += "\"cnn_prediction_one_probability\":\""+str(cnn_predictions.prediction_one_probability)+"\","
        response += "\"cnn_prediction_two\":\""+cnn_predictions.prediction_two+"\","
        response += "\"cnn_prediction_two_probability\":\""+str(cnn_predictions.prediction_two_probability)+"\","
        response += "\"cnn_prediction_three\":\""+cnn_predictions.prediction_three+"\","
        response += "\"cnn_prediction_three_probability\":\""+str(cnn_predictions.prediction_three_probability)+"\","
        response += "\"bert_prediction_one\":\""+bert_predictions.prediction_one+"\","
        response += "\"bert_prediction_one_probability\":\""+str(bert_predictions.prediction_one_probability)+"\","
        response += "\"bert_prediction_two\":\""+bert_predictions.prediction_two+"\","
        response += "\"bert_prediction_two_probability\":\""+str(bert_predictions.prediction_two_probability)+"\","
        response += "\"bert_prediction_three\":\""+bert_predictions.prediction_three+"\","
        response += "\"bert_prediction_three_probability\":\""+str(bert_predictions.prediction_three_probability)+"\""
        response += "}"
        return response
    else:
        response = "{"
        response += "\"lstm_prediction_one\":\""+lstm_predictions.prediction_one+"\","
        response += "\"lstm_prediction_one_probability\":\""+str(lstm_predictions.prediction_one_probability)+"\","
        response += "\"lstm_prediction_two\":\""+lstm_predictions.prediction_two+"\","
        response += "\"lstm_prediction_two_probability\":\""+str(lstm_predictions.prediction_two_probability)+"\","
        response += "\"lstm_prediction_three\":\""+lstm_predictions.prediction_three+"\","
        response += "\"lstm_prediction_three_probability\":\""+str(lstm_predictions.prediction_three_probability)+"\","
        response += "\"bert_prediction_one\":\""+bert_predictions.prediction_one+"\","
        response += "\"bert_prediction_one_probability\":\""+str(bert_predictions.prediction_one_probability)+"\","
        response += "\"bert_prediction_two\":\""+bert_predictions.prediction_two+"\","
        response += "\"bert_prediction_two_probability\":\""+str(bert_predictions.prediction_two_probability)+"\","
        response += "\"bert_prediction_three\":\""+bert_predictions.prediction_three+"\","
        response += "\"bert_prediction_three_probability\":\""+str(bert_predictions.prediction_three_probability)+"\""
        response += "}"
        return response
        

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

# bind only to localhost port 8644
# deployment environment handles HTTPS and proxy rules
app.run(port=8644)